
### Dead simple repository intialized with Create React App

`npx create-react-app poc-react-virtualized`

`yarn add react-virtualized`

Then I updated the sample app to use `src/List.js` which code comes from:
https://github.com/bvaughn/react-virtualized/blob/master/source/List/List.example.js

Data is mocked using `src/Brands.js`

**When I scroll, the content quickly become flickering, then no content is displayed at all.**

I striped it to bare bones to reduce any unrelated side effect (other library, etc), without seeing any improvment.