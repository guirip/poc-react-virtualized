import React, { Component } from 'react';
import List from './List';

import './App.css';

class App extends Component {
  render() {
    return (
        <div style={{ height: '100%', width: '100%' }}>
            <List />
        </div>
    );
  }
}

export default App;
