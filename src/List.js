
import * as React from 'react';

import List from 'react-virtualized/dist/commonjs/List';

// Array of data
import items from './Brands';


export default class ListExample extends React.PureComponent {

  constructor(props, context) {
    super(props, context);

    this.state = {
      listHeight: 300,
      listRowHeight: 50,
      elementHeight: 30,
      overscanRowCount: 10,
      scrollToIndex: undefined,
      elementStyle: {
        height: 30,
        listStyle: 'none',
      }
    };

    this._noRowsRenderer = this._noRowsRenderer.bind(this);
    this._onRowCountChange = this._onRowCountChange.bind(this);
    this._onScrollToRowChange = this._onScrollToRowChange.bind(this);
    this._rowRenderer = this._rowRenderer.bind(this);
  }

  render() {
    const rowCount = Array.isArray(items) ? items.length : 0;

    const {
      overscanRowCount,
      scrollToIndex,
      elementStyle,
    } = this.state;

    return (
        <List
            ref="List"
            className={""}
            height={window.innerHeight}
            overscanRowCount={overscanRowCount}
            noRowsRenderer={this._noRowsRenderer}
            rowCount={rowCount}
            rowHeight={elementStyle.height}
            rowRenderer={this._rowRenderer}
            scrollToIndex={scrollToIndex}
            width={window.innerWidth}
            />
    );
  }

  _noRowsRenderer() {
    return <div>No rows</div>;
  }

  _rowRenderer = ({ index, isScrolling, key, style }) => {
    let item = items[index];

    return (
      <li key={key} style={this.state.elementStyle}>{item.title}</li>
    );
  }

  _onRowCountChange(event) {
    const rowCount = parseInt(event.target.value, 10) || 0;

    this.setState({rowCount});
  }

  _onScrollToRowChange(event) {
    const {rowCount} = this.state;
    let scrollToIndex = Math.min(
      rowCount - 1,
      parseInt(event.target.value, 10),
    );

    if (isNaN(scrollToIndex)) {
      scrollToIndex = undefined;
    }

    this.setState({scrollToIndex});
  }

}
